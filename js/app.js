
// Maynards Site func :::::::::::::::::::::::::::::

var App = function(){

	$(document).foundation();

	// BIND FASTCLICK TO THE DELAYS ==============
	window.addEventListener('load', function () {
		FastClick.attach(document.body);
	}, false);

	console.log("site init")

}

// Kick things off
var app = new App();